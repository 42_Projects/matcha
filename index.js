'use strict'
//------------------------------------require-----------------------------------
var port			= 3000
var config			= require ('./config/config')
var express			= require ('express')
var session			= require ('express-session')
var cookieParser	= require ('cookie-parser')
var http			= require ('http')
var hash			= require ('password-hash')
var formidable		= require ('express-formidable')
var util			= require ('util') // ???
var fs				= require ('fs')
var fse				= require ('fs-extra')
var log				= require ('./modules/log')
var sql				= require ('./modules/sql')
var tools			= require ('./modules/tools')
var mail			= require ('./modules/mail')
var app				= express ()
var server			= app.listen (3000)
var io				= require ('socket.io') (server)
var io_session		= require ('express-socket.io-session')
var router			= express.Router ()
//-----------------------------------config-------------------------------------
session = session ({
	secret: 'keyboard cat',
	resave: false,
	saveUninitialized: false,
	cookie: {
		maxAge: 1000 * 60 * 60 * 1.5 }
})

io.use (io_session (session), {
	autoSave: true
})
//-----------------------------------static-------------------------------------
app.use (express.static ('public'))
//------------------------------------------------------------------------------
app.set ('view engine', 'pug')
app.set ('views', __dirname + '/views/templates')
app.use (cookieParser ('secret'))
app.use (formidable.parse ({
	multiples: true,
	uploadDir: '/tmp/'
}))

app.use (session)
app.use (function (req, res, next) {
	if (req.url.indexOf ('/resources/') < 0 &&
		req.url.indexOf ('/css/') < 0 &&
		req.url.indexOf ('/js/') < 0 &&
		req.url.indexOf ('favicon') < 0 &&
		req.url.indexOf ('/http_request') < 0) {
		var ip = req.headers['x-forwarded-for'] ||
				req.connection.remoteAddress ||
				req.socket.remoteAddress ||
				req.connection.socket.remoteAddress
		log ('request', '[ ' + ip + ' ][ ' + req.url + ' ]' + (req.session.user_id ? '[ ' + req.session.user_id + ' ]' : ''))
	}

	next ()
})
app.use ('/', router)
//-----------------------------------routes-------------------------------------
require ('./routes/statics')			(router, io)
require ('./routes/matches')			(router, io)
require ('./routes/tags')				(router, io)
require ('./routes/profil')				(router, io)
require ('./routes/history')			(router, io)
require ('./routes/login')				(router, io)
require ('./routes/logout')				(router, io)
require ('./routes/setup')				(router, io)
require ('./routes/clear')				(router, io)
require ('./routes/delete_account')		(router, io)
require ('./routes/change_password')	(router, io)
require ('./routes/lost_password')		(router, io)
require ('./routes/registration')		(router, io)
require ('./routes/http_request')		(router, io)
require ('./routes/error')				(router, io)
require ('./routes/chat')				(router, io)
require ('./routes/admin')				(router, io)
require ('./routes/404')				(router, io)
//--------------------------------- -socket-------------------------------------
io.sockets.on ('connection', function (socket) {
	socket.on ('pseudo_exists', function (pseudo) {
		sql.get ('SELECT `Pseudo` FROM `User` WHERE `Pseudo` = ?', [pseudo], function (rows) {
			socket.emit ('pseudo_exists', rows.length > 0)
		})
	})

	socket.on ('mail_exists', function (mail) {
		sql.get ('SELECT `Mail` FROM `User` WHERE `Mail` = ?', [unescape (mail)], function (rows) {
			socket.emit ('mail_exists', rows.length > 0)
		})
	})

	socket.on ('delete_image', function (image_id) {
		if (!socket.handshake.session.user_id)
			return

		sql.set ('UPDATE `Pictures` SET `Image_' + image_id + '` = ? WHERE `User_id` = ?', [false, socket.handshake.session.user_id], function (lastID, changes) {
			if (image_id == '0')
				socket.handshake.session.user_infos.image_0 = false
			else if (image_id == '1')
				socket.handshake.session.user_infos.image_1 = false
			else if (image_id == '2')
				socket.handshake.session.user_infos.image_2 = false
			else if (image_id == '3')
				socket.handshake.session.user_infos.image_3 = false
			socket.handshake.session.save (function (err) {
				if (err)
					return console.error (err)
				fse.remove ('resources/profils/' + socket.handshake.session.user_id + '/image_' + image_id, function (err) {
					socket.emit ('remove_image', image_id)
				})
			})
		})
	})

	socket.on ('location', function (latlng) {
		if (!socket.handshake.session.user_id)
			return

		sql.set ('UPDATE `User` SET `Latitude` = ?, `Longitude` = ? WHERE `User_id` = ?', [latlng[0], latlng[1], socket.handshake.session.user_id], function (lastID, changes) {})
	})

	socket.on ('new_message', function (socket_id, message, chat_file_path) {
		if (!socket_id) return

		if (io.sockets.connected[socket_id]) {
			io.sockets.connected[socket_id].emit ('received_new_message', message, socket.handshake.session.user_infos.firstname, chat_file_path)
		}
	})

	socket.on ('set_socket_id', function () {
		sql.set ('UPDATE `User` SET `Socket_id` = ? WHERE `User_id` = ?', [socket.id, socket.handshake.session.user_id], function (lastID, changes) {
			if (changes <= 0) return
			socket.handshake.session.user_infos.socket_id = socket.id
			socket.handshake.session.save ()
		})
	})

	socket.on ('liking', function (socket_id) {
		if (!socket_id) return

		var is_liking = socket_id.split (';')
		socket_id = is_liking[1]
		is_liking = is_liking[0]
		if (io.sockets.connected[socket_id]) {
			io.sockets.connected[socket_id].emit ('like', is_liking, socket.handshake.session.user_infos.firstname)
		}
	})
})

var moment = require ('moment')
//-----------------------------------start--------------------------------------
app.listen(port, 'localhost', function () {
	log ('server', 'Server is running on ' + port + '.')
})
