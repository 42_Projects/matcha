var sql = require ('../modules/sql')
var log = require ('../modules/log')

module.exports.install_database = function () {
	sql.set ('CREATE TABLE IF NOT EXISTS `User` ( \
		`user_id` INTEGER PRIMARY KEY AUTOINCREMENT, \
		`creation` varchar NOT NULL, \
		`mail` varchar NOT NULL, \
		`firstname` varchar NOT NULL, \
		`lastname` varchar NOT NULL, \
		`pseudo` varchar NOT NULL, \
		`password` varchar NOT NULL, \
		`password_key` varchar DEFAULT NULL, \
		`longitude` float DEFAULT 0, \
		`latitude` float DEFAULT 0, \
		`city` varchar DEFAULT NULL, \
		`settings` INTEGER DEFAULT -1, \
		\
		`key` varchar DEFAULT NULL, \
		`like_key` varchar DEFAULT NULL, \
		`msg_key` varchar DEFAULT NULL, \
		`visit_key` varchar DEFAULT NULL, \
		`profil_path` varchar DEFAULT NULL, \
		`images_path` varchar DEFAULT NULL, \
		\
		`visit_history_count` INTEGER DEFAULT 0, \
		`visitPink_history_count` INTEGER DEFAULT 0, \
		`like_history_count` INTEGER DEFAULT 0, \
		`likePink_history_count` INTEGER DEFAULT 0, \
		`messages_count` INTEGER DEFAULT 0, \
		`photos_count` INTEGER DEFAULT 0, \
		\
		`birthday` varchar, \
		`gender` INTEGER, \
		`sexual_orientation` INTEGER DEFAULT 2, \
		`bio` varchar DEFAULT "", \
		`score` INTEGER DEFAULT 0, \
		`socket_id` varchar DEFAULT NULL, \
		`last_sign_of_life` INTEGER DEFAULT 0, \
		`last_connection` varchar, \
		`report` INTEGER DEFAULT 0 \
	)', sql.set ('CREATE TABLE IF NOT EXISTS `Pictures` ( \
		`pictures_id` INTEGER PRIMARY KEY AUTOINCREMENT, \
		`user_id` INTEGER, \
		`image_0` INTEGER, \
		`image_1` INTEGER, \
		`image_2` INTEGER, \
		`image_3` INTEGER \
	)', sql.set ('CREATE TABLE IF NOT EXISTS `Tags` ( \
		`tags_id` INTEGER PRIMARY KEY AUTOINCREMENT, \
		`title` varchar (15) NOT NULL, \
		`uses` INTEGER (12) DEFAULT 1 \
	)', sql.set ('CREATE TABLE IF NOT EXISTS `Tag` ( \
		`tag_id` INTEGER PRIMARY KEY AUTOINCREMENT, \
		`user_id` INTEGER, \
		`title` varchar NOT NULL \
	)', sql.set ('CREATE TABLE IF NOT EXISTS `Like` ( \
		`like_id` INTEGER PRIMARY KEY AUTOINCREMENT, \
		`creation` varchar NOT NULL, \
		`user_id` INTEGER, \
		`target_id` INTEGER, \
		`saw` INTEGER DEFAULT 0 \
	)', sql.set ('CREATE TABLE IF NOT EXISTS `Visit` ( \
		`visit_id` INTEGER PRIMARY KEY AUTOINCREMENT, \
		`creation` varchar NOT NULL, \
		`user_id` INTEGER, \
		`target_id` INTEGER, \
		`saw` INTEGER DEFAULT 0 \
	)', sql.set ('CREATE TABLE IF NOT EXISTS `Colors` ( \
		`color_id` INTEGER PRIMARY KEY AUTOINCREMENT, \
		`user_id` INTEGER, \
		`grey_1` varchar DEFAULT "#212121", \
		`grey_2` varchar DEFAULT "#303030", \
		`grey_3` varchar DEFAULT "#424242", \
		`grey_4` varchar DEFAULT "#606060" \
	)', sql.set ('CREATE TABLE IF NOT EXISTS `Report` ( \
		`report_id` INTEGER PRIMARY KEY AUTOINCREMENT, \
		`creation` varchar NOT NULL, \
		`user_id` INTEGER NOT NULL, \
		`target_id` INTEGER NOT NULL \
	)', sql.set ('CREATE TABLE IF NOT EXISTS `Block` ( \
		`block_id` INTEGER PRIMARY KEY AUTOINCREMENT, \
		`creation` varchar NOT NULL, \
		`user_id` INTEGER NOT NULL, \
		`target_id` INTEGER NOT NULL \
	)', sql.set ('CREATE TABLE IF NOT EXISTS `Chat` ( \
		`chat_id` INTEGER PRIMARY KEY AUTOINCREMENT, \
		`creation` varchar NOT NULL, \
		`user_id` INTEGER NOT NULL, \
		`target_id` INTEGER NOT NULL, \
		`file_path` varchar NOT NULL, \
		`user_unread` INTEGER DEFAULT 0, \
		`target_unread` INTEGER DEFAULT 0, \
		`enabled` INTEGER DEFAULT 1 \
	)', function (err) {
		log ('database', 'Database installed.')
	}))))))))))
}

module.exports.clear_database = function () {
	sql.set ('DROP TABLE IF EXISTS `User`, `Profil`, `Pictures`, `Tags`, `Tag`, `Like`, `Visit`', function () {
		log ('database', 'Database cleared.')
	})
}
