var config		= require ('../config/config')
var mailer		= require ('nodemailer')
var log			= require ('../modules/log')
var transporter	= mailer.createTransport ('smtps://' + config.MAIL_user + '@gmail.com:' + config.MAIL_pass + '@smtp.gmail.com')

/*
**	send mail
*/
exports.send = function (to, subject, html) {
	var options = {
		from: 'Matcha',
		to: to,
		subject: subject,
		html: html
	}

	transporter.sendMail (options, function (err, info) {
		if (err) {
			log ('error', 'sending mail | ' + err)
			return
		}
		log ('mail', 'message sent to ' + to + ' : ' + subject)
	})
}
