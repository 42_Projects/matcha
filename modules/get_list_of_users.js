var config	= require ('../config/config')
var sql		= require ('../modules/sql')
var hash	= require ('password-hash')
var moment	= require ('moment')

function get_distance (user_lat, user_lng, target_lat, target_lng) {
	const PI = Math.PI

	var dist_lat = (target_lat - user_lat) * (Math.PI / 180)
	var dist_lng = (target_lng - user_lng) * (Math.PI / 180)
	var d = Math.pow (Math.sin (dist_lat / 2.0), 2) + Math.cos (user_lat * (Math.PI / 180)) * Math.cos (target_lat * (Math.PI / 180)) * Math.pow (Math.sin (dist_lng / 2.0), 2)
	d = 2 * Math.atan (Math.sqrt (d), Math.sqrt (1 - d))

	return 6367 * d
}

module.exports = function (user_infos, query, args, callback) {
	var id_0 = []
	var id_1 = []
	var id_2 = []
	var id_3 = []

	sql.get (query, args, function (users) {
		if (!users || users.length == 0) return callback (null)

		for (var user in users) {
			id_1.push (user)

			users[user].distance = get_distance (user_infos.latitude, user_infos.longitude, users[user].latitude, users[user].longitude).toFixed (1)
			users[user].offline_since = users[user].last_sign_of_life == 0 ? ': Not connected yet' : moment ().to (moment (users[user].last_sign_of_life, 'x'))
			users[user].is_online = (Date.now () - users[user].last_sign_of_life) < (2.5 * 60 * 1000)
			users[user].like_key = users[user].like_key + ';' + hash.generate (users[user].like_key + config.HASH_like + user_infos.like_key, {
				algorithm: 'whirlpool'
			})
			sql.get ('SELECT `User_id`, `Target_id` FROM `Like` WHERE (`User_id` = ? AND `Target_id` = ?) OR (`User_id` = ? AND `Target_id` = ?)', [
				user_infos.user_id, users[id_1[id_1.length - 1]].user_id, users[id_1[id_1.length - 1]].user_id, user_infos.user_id
			], function (like_rows) {
				id_2.push (id_1.shift ())
				sql.get ('SELECT `User_id`, `Target_id` FROM `Visit` WHERE (`User_id` = ? AND `Target_id` = ?) OR (`User_id` = ? AND `Target_id` = ?)', [
					user_infos.user_id, users[id_2[id_2.length - 1]].user_id, users[id_2[id_2.length - 1]].user_id, user_infos.user_id
				], function (visit_rows) {
					id_3.push (id_2.shift ())
					sql.get ('SELECT * FROM `Tag` WHERE `User_id` = ?', [
						users[id_3[id_3.length - 1]].user_id
					], function (tag_rows) {
						var current_user = id_3.shift ()

						if (like_rows.length == 2) {
							users[current_user]['liked'] = true
							users[current_user]['target_liked'] = true
						} else if (like_rows.length == 1)
							users[current_user][like_rows[0].user_id == user_infos.user_id ? 'liked' : 'target_liked'] = true
						if (visit_rows.length == 2) {
							users[current_user]['visited'] = true
							users[current_user]['target_visited'] = true
						} else if (visit_rows.length == 1)
							users[current_user][visit_rows[0].user_id == user_infos.user_id ? 'visited' : 'target_visited'] = true
						users[current_user]['tags'] = tag_rows.length > 0 ? tag_rows : null
						users[current_user]['age'] = Math.floor ((new Date - new Date (users[current_user].birthday)) / 1000 / (60 * 60 * 24) / 365.25)
						if (id_3.length == 0) callback (users)
					})
				})
			})
		}
	})
}
