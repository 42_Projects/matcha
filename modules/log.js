const fs	= require ('fs')

/*
**	returns the date as yyyy-mm-dd hh:mm:ss
*/

function get_ip (req) {
	return req.connection.remoteAddress ||
		req.socket.remoteAddress ||
		req.connection.socket.remoteAddress
}

function get_date () {
	var date = new Date ()

	var hour = date.getHours ()
	hour = (hour < 10 ? '0' : '') + hour

	var min  = date.getMinutes()
	min = (min < 10 ? '0' : '') + min

	var sec  = date.getSeconds()
	sec = (sec < 10 ? '0' : '') + sec

//*	to get more precision
	var m_sec  = date.getMilliseconds()
	m_sec = (m_sec < 100 ? m_sec < 10 ? '00' : '0' : '') + m_sec
	sec = sec + '\'' + m_sec
//*/

	var year = date.getFullYear()

	var month = date.getMonth() + 1
	month = (month < 10 ? '0' : '') + month

	var day  = date.getDate()
	day = (day < 10 ? '0' : '') + day

	return year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec
}

/*
**	write `data` on console.log and /logs/`type`.log
*/
module.exports = function (type, data, callback) {
	const path = require('path').dirname(require.main.filename)

	console.log (type + ' -> ' + data)
	fs.appendFile (path + '/logs/' + type + '.log', get_date () + ' -> ' + data + '\n', callback)
}
