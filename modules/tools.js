module.exports.get_url = function (req) {
	return (req.protocol + '://' + req.get ('host') + req.originalUrl)
}

module.exports.parse_params = function (args, callback) {
	var args_array = []
	args = args.split ('&')

	for (var id in args) {
		var pair = args[id].split ('=')
		args_array[pair[0]] = pair[1]
	}

	if (callback)
		callback ()

	return (args_array)
}

module.exports.array_to_string = function (array, callback) {
	var s = ''

	for (var k in array) {
		if (array.hasOwnProperty(k)) {
			if (s)
				s += ';'
			s += k + ':' + array[k]
		}
	}

	if (callback)
		callback ()

	return (s)
}

module.exports.string_to_array = function (s, callback) {
	var array = []
	var infos = s.split (';')

	for (var info in infos) {
		if (infos.hasOwnProperty(info)) {
			var pair = infos[info].split (':')
			array[pair[0]] = pair[1]
		}
	}

	if (callback)
		callback ()

	return (array)
}
