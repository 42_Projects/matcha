var sql		= require ('sqlite3')
var config	= require ('../config/config')
var log		= require ('../modules/log')
var db		= new sql.Database (require('app-root-path') + '/databases/matcha.db')

module.exports.get = function (query, args, callback) {
	if (arguments.length == 2) {
		callback = arguments[1]
		args = {}
	}

	for (var arg in args) {
		if (args.hasOwnProperty(arg)) {
			arg = escape (arg)
		}
	}

	db.all (query, args, function (err, rows) {
		if(err) {
			log ('error', 'pool querying' + err)
			return
		}
		if (callback) {
			callback (rows)
		}
	})
}

module.exports.set = function (query, args, callback) {
	if (arguments.length == 2) {
		callback = arguments[1]
		args = {}
	}

	for (var arg in args) {
		if (args.hasOwnProperty(arg)) {
			arg = escape (arg)
		}
	}

	db.run (query, args, function (err) {
		if (err)
			return console.error (err)
		if (callback)
			callback (this.lastID, this.changes)
	})
}
