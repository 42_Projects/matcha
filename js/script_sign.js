var already_exists = 0b0

function need_to_be_filled (item, text) {
	item.css ('box-shadow', 'var(--shadow)' + ', ' + 'inset 0 0 5px red')
	item.attr ('placeholder', item.attr ('placeholder').split (' | ')[0] + ' | ' + text)
	item.val ('')
	return false
}

function validate_form_lost_password () {
	var flag = true
	var mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

	$('div.element#lost_password input').each(function () {
		var input = $(this)

		if ($(this).val() == '')
			flag = need_to_be_filled ($(this), 'Required *', flag)
		switch (input.attr ('name')) {
			case 'mail':
				if (!mail.test (input.val ()))
					flag = need_to_be_filled (input, 'Please enter a valid mail *', flag)
				break;
			default:
		}
	})
	return flag
}

function validate_form_change_password () {
	var flag = true
	var password = /^.{4,50}$/

	$('div.element#change_password input').each(function () {
		var input = $(this)

		if ($(this).val() == '')
			flag = need_to_be_filled ($(this), 'Required *', flag)
		switch (input.attr ('name')) {
			case 'password':
			case 'password_verification':
				if (!password.test (input.val ()))
					flag = need_to_be_filled (input, 'The password must contains at least 4 characters *', flag)
				break;
			default:
		}
	})
	if ($('input[name = \'pass_1\']').val() != $('input[name = \'pass_2\']').val()) {
		flag = need_to_be_filled ($('input[name = \'pass_1\']'), 'These passwords don\'t match.', flag)
		flag = need_to_be_filled ($('input[name = \'pass_2\']'), 'These passwords don\'t match.', flag)
	}
	return flag
}

function validate_form_connection () {
	var flag = true
	var mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
	var pseudo = /^[a-zA-Z0-9_-]{3,15}$/

	$('div.sub_element#connection input').each(function () {
		var input = $(this)

		if (input.val() == '')
		flag = need_to_be_filled ($(this), 'Required *', flag)
		switch (input.attr ('name')) {
			case 'pseudo':
				if (!mail.test (input.val ()) && !pseudo.test (input.val ()))
					flag = need_to_be_filled (input, 'This pseudo/mail is not registered', flag)
				break;
			default:
		}
	})
	return flag
}



/*
	Function called before sending the registration form.
	For each input, checks if it's empty, matches with regex.
	For pseudo and mail, check if value is already on database.
*/
function validate_form_registration () {
	var flag = true
	/*
		Defines regex for the inputs.
	*/
	var mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
	var pseudo = /^[a-zA-Z0-9_-]{3,15}$/
	var password = /^.{4,50}$/
	var name = /^([A-Z]?[a-z]{2,15}-?[A-Z]?[a-z]{2,15})$|^([A-Z]?[a-z]{2,34})$/

	$('div.element#registration input').each(function () {
		var input = $(this)

		if (input.attr ('name') == 'gender' || input.attr ('type') == 'submit')
			return
		if (input.val() == '')
			flag = need_to_be_filled (input, 'Required *', flag)
		else {
			switch (input.attr ('name')) {
				case 'pseudo':
					if (!pseudo.test (input.val ()))
						flag = need_to_be_filled (input, 'Only a-z, A-Z, 0-9, - and _ are accepted *', flag)
					break;
				case 'birthday':
					if ((new Date - new Date (input.val ())) / 1000 / (60 * 60 * 24) / 365.25 < 18) {
						alert ('You have to be major. Sorry.')
						flag = false
					}
					break
				case 'mail':
					if (!mail.test (input.val ()))
						flag = need_to_be_filled (input, 'Please enter a valid mail *', flag)
					break
				case 'firstname':
					if (!name.test (input.val ()))
						flag = need_to_be_filled (input, 'Please enter a valid firstname *', flag)
					break
				case 'lastname':
					if (!name.test (input.val ()))
						flag = need_to_be_filled (input, 'Please enter a valid lastname *', flag)
					break
				case 'password':
				case 'password_verification':
					if (!password.test (input.val ()))
						flag = need_to_be_filled (input, 'The password must contains at least 4 characters *', flag)
					break
				default:
			}
		}
	})
	/*
		Checks if the two passwords are identicals.
	*/
	if ($('input[name = \'pass_1\']').val() != $('input[name = \'pass_2\']').val()) {
		flag = need_to_be_filled ($('input[name = \'pass_1\']'), 'These passwords don\'t match.', flag)
		flag = need_to_be_filled ($('input[name = \'pass_2\']'), 'These passwords don\'t match.', flag)
	}
	return flag && !already_exists
}

$(document).ready (function () {
	/*
		Options for toastr
	*/
	toastr.options.preventDuplicates = true
	toastr.options.timeOut = 50000
	toastr.options.extendedTimeOut = 0
	toastr.options.closeDuration = 30

	/*
		Hide registration's tile and toggle it on click
	*/
	$("div.element#registration").hide ()
	$('div.sub_element#registration_button input').click (function(){
		$('div.element#registration').slideToggle()
		if ($('div.element#registration').is (':visible')) {
			setTimeout (function () {
				$('div.element#registration input[name = \'pseudo\']').focus ()
			}, 500);
		}
	})

	$('div#registration input').focusin (function() {
		$(this).css ('box-shadow', 'var(--shadow)')
	})

	$('div#connection input').focusin(function () {
		$(this).css ('box-shadow', 'var(--shadow)')
	})

	$('div#connection input[name = \'pseudo\']').focusin (function () {
		setTimeout (function () {
			toastr.error ('Click here!', 'You forgot your password ?', {
				onclick: function () {
					document.location.href = '/lost_password'
				},
				extendedTimeOut: -1,
				timeOut: -1
			})
		}, 2000)
	})

	$('div#connection input[name = \'pseudo\']').focus ()

	var pseudo_input = document.querySelector ('div.element#registration input[name = \'pseudo\']')
	var mail_input = document.querySelector ('div.element#registration input[name = \'mail\']')

	var socket = io.connect ()

	/*
		If the pseudo already exists:
			- display a toast
			- set the color of the pseudo to red
	*/
	socket.on ('pseudo_exists', function (pseudo_exists) {
		if (pseudo_exists) {
		console.log ('exists')
			toastr.error ('This pseudo already exists. Please choose an other one !')
			pseudo_input.style.color = 'red'
			already_exists |= 0b10
		} else {
			pseudo_input.style.color = 'black'
			already_exists &= 0b01
		}
	})

	socket.on ('mail_exists', function (pseudo_exists) {
		console.log ('exists')
		if (pseudo_exists) {
		console.log ('exists')
			toastr.error ('An account already exists with this mail.')
			mail_input.style.color = 'red'
			already_exists |= 0b01
		} else {
			mail_input.style.color = 'black'
			already_exists &= 0b10
		}
	})

	if (pseudo_input) {
		pseudo_input.addEventListener ('input', function () {
			socket.emit ($(this).attr ('name') + '_exists', $(this).val ())
		})
		pseudo_input.addEventListener ('focusout', function () {
			socket.emit ($(this).attr ('name') + '_exists', $(this).val ())
		})
	}

	if (mail_input) {
		mail_input.addEventListener ('keyup', function () {
			socket.emit ($(this).attr ('name') + '_exists', $(this).val ())
		})
	}
})
