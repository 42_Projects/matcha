function need_to_be_filled (item, text) {
	item.boxShadow = 'var(--shadow)' + ', ' + 'inset 0 0 5px red'
	item.placeholder = item.placeholder.split (' | ')[0] + ' | ' + text
	item.value = ''

	return false
}

/*
Function called before sending the updating profil form.
For each input, checks if it's empty, matches with regex.
For pseudo and mail, check if value is already on database.
*/
function validate_form_profil() {
	var flag = true
	/*
	Defines regex for the inputs.
	*/
	var password = /^(.{0}|.{4,50})$/

	document.querySelectorAll ('div.element#profil input').forEach (function (input) {
		if (input.name == 'gender' || input.type == 'submit') return

		switch (input.name) {
			case 'password':
			case 'password_verification':
				if (!password.test (input.value))
					flag = need_to_be_filled (input, 'The password must contains at least 4 characters *', flag)
			break;
			default:
		}
	})

	if (document.querySelector ('input[name = \'password\']').value != document.querySelector ('input[name = \'password_verification\']').value) {
		flag = need_to_be_filled(document.querySelector ('input[name = \'password\']'), 'These passwords don\'t match.', flag)
		flag = need_to_be_filled(document.querySelector ('input[name = \'password_verification\']'), 'These passwords don\'t match.', flag)
	}
	return flag
}

window.onload = function () {
	var request = createRequest()

	history_buttons ()

	document.querySelectorAll ('tr#upload_tr input[type = \'file\']').forEach (function (div) {
		div.addEventListener ('change', function (e) {
			if (div.files && div.files[0]) {
				var reader = new FileReader ()
				var image = div
				reader.onload = function (e) {
					console.log (image.result)
					document.querySelector ('tr#upload_tr img#image_' + image.id).src = e.target.result
					document.querySelector ('tr#upload_tr label#label_' + image.id).innerHTML = 'Modify'
				}
				reader.readAsDataURL (this.files[0])
			}
		})
	})

	document.querySelectorAll ('tr#upload_tr div.delete_image').forEach (function (div) {
		div.addEventListener ('click', function () {
			var request = createRequest()

			getRequest(request, '/http_request/delete_image', {
				id: div.id.split('_')[1]
			})

			httpResponse(request, function(response) {
				window.location.href = '/profil'
			})
		})
	})

	var marker = null

	var map = new google.maps.Map(document.getElementById("carte"), {
		zoom: 15,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	})

	google.maps.event.addListener(map, 'click', function(event) {
		placeMarker(event.latLng, {
			updateDb: true
		})
	})

	document.getElementById ('locate').addEventListener ('click', function () {
		$.getJSON('http://ipinfo.io', function(data) {
			var latlng = data.loc.split(',')

			if (parseInt(latlng[0]) < -60) latlng[0] = '-60'
			if (parseInt(latlng[0]) > 84) latlng[0] = '84'

			placeMarker(new google.maps.LatLng(latlng[0], latlng[1]), {
				centerMap: true,
				updateDb: true
			})
		})
	})

	getRequest(request, '/http_request/get_location')
	httpResponse(request, function(response) {
		var latlng = response.split(';')
		if (parseInt(latlng[0]) < -60) latlng[0] = '-60'
		if (parseInt(latlng[0]) > 84) latlng[0] = '84'
		if (latlng[0] == 0 && latlng[1] == 0)
			$('div#locate').click ()
		else {
			placeMarker(new google.maps.LatLng(latlng[0], latlng[1]), {
				centerMap: true
			})
		}
	})

	function placeMarker(location, options) {
		if (marker)
			marker.setMap(null)
		marker = new google.maps.Marker({
			position: location
		})
		marker.setMap(map)
		if (options.centerMap)
			map.setCenter(location)
		if (options.updateDb) {
			var request = createRequest()

			getRequest(request, '/http_request/set_location', {
				'lat': location.lat(),
				'lng': location.lng()
			})
			httpResponse(request, function(response) {})
		}
	}

	document.addEventListener ('keyup', function (event) {
		if (event.target.className == 'tags') {
			if (event.target.value == '' && event.keyCode != 13) {
				if (event.target.parentElement.previousSibling) {
					event.target.parentElement.previousSibling.children[0].focus ()
					event.target.parentElement.previousSibling.children[0].value = event.target.parentElement.previousSibling.children[0].value
				} else if (event.target.parentElement.nextSibling) {
					event.target.parentElement.nextSibling.children[0].focus ()
					event.target.parentElement.nextSibling.children[0].value = event.target.parentElement.nextSibling.children[0].value
				}
				if (event.target.parentElement.parentElement.children.length > 1)
 					event.target.parentElement.remove ()
			}
		}
	})

	document.addEventListener ('keydown', function (event) {
		if (event.target.className == 'tags') {
			if (!/^\#[a-zA-Z]{0,10}$/.test (event.target.value)) {
				event.target.value = event.target.value.replace (/^[a-zA-Z]{0,10}$/g, '#$&')
			}
			if (event.target.value != '' && event.target.value != '#' && event.keyCode == 13) {
				event.preventDefault ()
				var new_tag = $('<input></input>').attr ({
					type: 'text',
					class: 'tags',
					placeholder: 'tags',
					name: 'tags',
					value: '#'
				})
				$('tr#tags tr').append ($('<td></td>').append (new_tag))
				new_tag.focus ()
				new_tag.val ('#')
			}
		}
	})
}
