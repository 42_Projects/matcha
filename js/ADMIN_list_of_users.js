function validate_user (item, user_id) {
	var request = createRequest ()

	getRequest (request, '/http_request/validate_user', {
		user_id: user_id
	})
	httpResponse (request, function (response) {
		if (response == 'done')
			item.firstChild.src = 'resources/icons/checkbox.png'
	})
}

function delete_account (item, user_id) {
	var request = createRequest ()

	getRequest (request, '/http_request/delete_account', {
		user_id: user_id
	})
	httpResponse (request, function (response) {
		if (response == 'done') {
			var to_remove = item.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode
			to_remove.parentNode.removeChild (to_remove)
		}
	})
}

window.onload = history_buttons
