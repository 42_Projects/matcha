// DONE #NO JQUERY

var offset = 0
var limit = 5
var need_more_users = true

var min_age = 18
var max_age = 60

var min_score = 0
var max_score = 100000

var min_distance = 0
var max_distance = 21000

var is_searching = false

window.onload = function () {
	load_search ()
	history_buttons ()

	document.addEventListener ('click', function (event) {
		if (event.target.className.indexOf ('like') <= 0 && event.target.parentElement.className.indexOf ('like') <= 0) return

		var target = event.target.className.indexOf ('like') > 0 ? event.target : event.target.parentElement
		var image = target.firstChild
		var is_pink = image.src.indexOf ('pink') > 0
		var score = target.parentElement.parentElement.querySelector ('tr.score td:last-child')

		if (image.src.indexOf ('favorite' + (is_pink ? '_pink' : '') + '.png') > 0) {
			score.innerHTML = parseInt (score.innerHTML) - 10
			image.src = 'resources/icons/favorite_border' + (is_pink ? '_pink' : '') + '.png'
		}
		else {
			score.innerHTML = parseInt (score.innerHTML) + 10
			image.src = 'resources/icons/favorite' + (is_pink ? '_pink' : '') + '.png'
		}

		var request = createRequest ()

		getRequest (request, '/http_request/like', {
			'key': target.id
		})
		httpResponse (request, function (response) {
			if (!response) return

			socket.emit ('liking', response)
		})
	})

	document.querySelector ('#select_tag').addEventListener ('change', function (event) {
		document.getElementById ('tags_looked_for').value += (document.getElementById ('tags_looked_for').value.length > 0 ? ', ' : '') + event.target.value.split ('#')[1]
		event.target.selectedIndex = 0
		search ()
	})

	var radios = document.querySelectorAll ('input[type = "radio"]')
	if (radios) {
		for (var radio in radios) {
			if (radios.hasOwnProperty(radio)) {
				radios[radio].addEventListener ('click', function () {
					search ()
				})
			}
		}
	}

	/* LOADING THE FIRSTS ELEMENTS */
	var request = createRequest ()

	getRequest (request, '/http_request/get_matches', {
		limit: limit,
		offset: offset

	})

	httpResponse (request, function (response) {
		if (!response || response.length == 0)
			return need_more_users = false

		offset += limit
		document.getElementById ('main').innerHTML += response
	})

	min_age = age - 4
	max_age = age + 4
}

window.addEventListener ('scroll', function () {
	var body = document.body
	var html = document.documentElement
	var height = Math.max (body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight)

	if (need_more_users && window.scrollY == height - window.innerHeight) {
		var request = createRequest ()

		getRequest (request, '/http_request/get_matches', {
			limit: limit,
			offset: offset,
			min_age: is_searching ? min_age : null,
			max_age: is_searching ? max_age : null,
			min_score: is_searching ? min_score : null,
			max_score: is_searching ? max_score : null,
			min_distance: is_searching ? min_distance : null,
			max_distance: is_searching ? max_distance : null,
			tags: is_searching ? document.getElementById ('tags_looked_for').value : null,
			order_by: is_searching ? document.querySelector('input[name = "order"]:checked').value : null,
			order_by_sens: is_searching ? document.querySelector('input[name = "sens"]:checked').value : null
		})

		httpResponse (request, function (response) {
			var loader = document.getElementsByClassName('loader')[0]
			if (loader)
				loader.parentNode.removeChild(loader)

			if (!response || response.length == 0)
				return need_more_users = false

			offset += limit
			document.getElementById ('main').innerHTML += response
		})
	}
})

function toggle_search () {
	if (document.getElementById ('search').className.indexOf ('hidden_search') >= 0)
		search ()
	document.getElementById ('search').classList.toggle ('hidden_search')
	is_searching = document.getElementById ('search').className.indexOf ('hidden_search') < 0
}
