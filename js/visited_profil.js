window.onload = function () {
	var request = createRequest ()

	var map = new google.maps.Map(document.getElementById ('carte'), {
		zoom: 15,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	})

	history_buttons ()

	getRequest (request, '/http_request/get_location_of')
	httpResponse (request, function(response) {
		var latlng = response.split (';')
		if (parseInt (latlng[0]) < -60) latlng[0] = '-60'
		if (parseInt (latlng[0]) > 84) latlng[0] = '84'
		if (latlng[0] == 0 && latlng[1] == 0)
			$('div#locate').click ()
		else {
			placeMarker(new google.maps.LatLng (latlng[0], latlng[1]), {
				centerMap: true
			})
		}
	})

	function placeMarker(location, options) {
		var marker = new google.maps.Marker({
			position: location
		})
		marker.setMap (map)
		if (options.centerMap)
			map.setCenter(location)
	}

	var report = document.getElementById ('report')
	report.addEventListener ('click', function () {
		if (!confirm ('Are you sure you want to block this user ?')) return

		getRequest (request, '/http_request/report')
		httpResponse (request, function (response) {
			if (response)
				window.location = '/'
		})
	})

	var block = document.getElementById ('block')
	block.addEventListener ('click', function () {
		if (!confirm ('Are you sure you want to block this user ?')) return

		getRequest (request, '/http_request/block')
		httpResponse (request, function (response) {
			if (response)
				window.location = '/'
		})
	})
}
