var socket = io.connect ()

var delay = 2 * 60 * 1000
var request = createRequest ()

getRequest (request, '/http_request/im_alive', [])
httpResponse (request, function (response) {})

window.setInterval (function () {
	getRequest (request, '/http_request/im_alive', [])
}, delay)

function history_buttons () {
	socket.emit ('set_socket_id')

	var history_buttons = document.querySelector ('div.nav_button_container:first-child')

	document.querySelector ('div#history').addEventListener ('mouseover', function (event) {
		history_buttons.classList.add ('history')
	})

	document.querySelector ('nav').addEventListener ('mouseleave', function (event) {
		history_buttons.classList.remove ('history')
	})
}

// When someone like this user
socket.on ('like', function (is_liking, pseudo) {
	var favorite_pink_tag = document.getElementById ('favorite_pink').children[1]
	var history_tag = document.getElementById ('history').children[1]
	var last_value = favorite_pink_tag.innerHTML

	if (is_liking == 'true' || is_liking == '2') {
		favorite_pink_tag.innerHTML = parseInt (last_value) + 1
		history_tag.innerHTML = parseInt (history_tag.innerHTML) + 1
		if (last_value == 0) {
			favorite_pink_tag.classList.remove ('hidden')
			history_tag.classList.remove ('hidden')
		}
		toastr.success (pseudo + ' liked you ' + (is_liking == '2' ? 'too ' : '') + '!')

		getRequest (request, '/http_request/update_likePink_count', {update: '1'})
	}
	else {
		toastr.error (pseudo + ' unliked you !')
	}
})

socket.on ('visit', function (pseudo) {
	var visit_pink_tag = document.getElementById ('viewed_pink').children[1]
	var history_tag = document.getElementById ('history').children[1]
	var last_value = visit_pink_tag.innerHTML

	visit_pink_tag.innerHTML = parseInt (last_value) + 1
	history_tag.innerHTML = parseInt (history_tag.innerHTML) + 1
	if (last_value == 0) {
		visit_pink_tag.classList.remove ('hidden')
		history_tag.classList.remove ('hidden')
	}
	toastr.success (pseudo + ' visited your profil!')

	getRequest (request, '/http_request/update_visitPink_count', {update: '1'})
})

socket.on ('received_new_message', function (_, pseudo, chat_file_path) {
	toastr.success (pseudo + ' sent you a message !')

	console.log ('1')
	if (window.location.pathname.split ('/chat/')[1] == chat_file_path) return
	console.log ('2')

	var messages_tag = document.getElementById ('messages').children[1]
	var last_value = messages_tag.innerHTML

	messages_tag.innerHTML = parseInt (last_value) + 1
	if (messages_tag.classList.contains ('hidden'))
		messages_tag.classList.remove ('hidden')

	getRequest (request, '/http_request/update_messages_count', {update: '1', file_path: chat_file_path})
})
