function load_search () {
	$('#slider_age').slider ({
		range: true,
		min: 18,
		max: 60,
		values: [age - 4, age + 4],
		slide: function (event, ui) {
			min_age = ui.values[0]
			max_age = ui.values[1]
			$('#min_age').text (min_age + ' years')
			$('#max_age').text (max_age + ' years')
		},
		change: function () {
			search ()
		}
	})

	$('#slider_score').slider ({
		range: true,
		min: 2,
		max: 5,
		step: 0.001,
		values: [2, 5],
		slide: function (event, ui) {
			min_score = Math.round (parseInt(Math.pow (10, ui.values[0]) * (ui.values[0] != 2) / 100) * 100)
			max_score = Math.round (parseInt(Math.pow (10, ui.values[1]) / 100) * 100)
			$('#min_score').text (min_score + ' pts')
			$('#max_score').text (max_score + ' pts')
		},
		change: function () {
			search ()
		}
	})

	$('#slider_distance').slider ({
		range: true,
		min: 0,
		max: 4,
		step: 0.001,
		values: [0, 4],
		slide: function (event, ui) {
			min_distance = Math.round (parseInt(Math.pow (10, ui.values[0]) * (ui.values[0] != 0) / 1) * 1)
			max_distance = ui.values[1] == 4 ? 21000 : Math.round (parseInt(Math.pow (10, ui.values[1]) / 1) * 1)
			$('#min_distance').text (min_distance + ' km')
			$('#max_distance').text (max_distance == 21000 ? 'Everywhere' : max_distance + ' km')
		},
		change: function () {
			search ()
		}
	})

	document.getElementById ('tags_looked_for').addEventListener ('keyup', function (e) {
		if (e.keyCode == 188 || e.keyCode == 13)
			search ()
	})
}

function search () {

	if (!document.querySelector('input[name = "order"]:checked') ||
		!document.querySelector('input[name = "sens"]:checked'))
		return

	getRequest (request, '/http_request/get_matches', {
		limit: limit,
		offset: 0,
		min_age: min_age,
		max_age: max_age,
		min_score: min_score,
		max_score: max_score,
		min_distance: min_distance,
		max_distance: max_distance,
		tags: document.getElementById ('tags_looked_for').value,
		order_by: document.querySelector('input[name = "order"]:checked').value,
		order_by_sens: document.querySelector('input[name = "sens"]:checked').value
	})

	httpResponse (request, function (response) {
		offset = 5

		$('div#main').html ('')
		$('div#main').append (response)
	})
}
