function arrayToQuery (array) {
	var query = ''

	for (var key in array) {
		if (array.hasOwnProperty(key)) {
			if (query != '')
				query += '&'
			query += key + '=' + array[key]
		}
	}
	return query
}

function postRequest (xhr, path, args, callback) {
	xhr.open ('POST', path, true)
	xhr.setRequestHeader ('Content-Type', 'application/x-www-form-urlencoded')
	xhr.send (arrayToQuery (args))

	if (callback) callback ()
}

function getRequest (xhr, path, args, callback) {
	xhr.open ('GET', path + '?' + arrayToQuery (args), true)
	xhr.send (null)

	if (callback) callback ()
}

function httpResponse (xhr, callback) {
	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			if (callback)
				callback (xhr.responseText)
		}
	}
}

function createRequest () {
	return new XMLHttpRequest ()
}

/*
	var request = createRequest ()

	getRequest (request, path, {
		var1: value,
		var2: value
	})
	httpResponse (request, function (response) {

	})
*/
