window.onload = function(event) {
	var chat = document.getElementById ('chat')
	var textarea = document.getElementById ('textarea')

	history_buttons ()

	// Scroll to the last messages;
	chat.scrollTop = chat.scrollHeight

	textarea.addEventListener ('keyup', function (e) {
		// If the user presses `Enter`, a click on the submit button is triggered;
		if (!e.ctrlKey && e.keyCode == 13) {
			document.querySelector ('#dialog_input input').click ()
			textarea.value = ''
		}
		// If the user presses `Enter` + `Ctrl`, a new line is inserted;
		else if (e.ctrlKey && e.keyCode == 13)
			textarea.value = textarea.value + '\n'
	})

	// Gives the focus to the textarea when the page load;
	textarea.focus ()

	// When the user send a message;
	document.getElementById ('send_message').addEventListener ('click', function () {
		var message = textarea.value
		var message_bubble = document.createElement ('p')
		var last_child = document.querySelector ('div#chat div:last-of-type')

		// Break the function if the message is empty;
		if (!message || /^(\s|\n)+$/.test (message)) return false

		message_bubble.innerHTML = message
		textarea.value = ''

		if (!last_child || (last_child && last_child.className == 'target')) {
			last_child = document.createElement ('div')
			last_child.className = 'user'
			last_child.appendChild (document.getElementById ('user_image').cloneNode ())
		}

		last_child.appendChild (message_bubble)
		chat.insertBefore (last_child, document.getElementById ('empty'))

		scrollToTop (document.getElementById ('chat'), 1000)

		var request = createRequest ()

		getRequest (request, '/http_request/add_message', {
			message: message
		})

		httpResponse (request, function (response) {
			if (!response) return

			socket.emit ('new_message', response, message, window.location.pathname.split ('/chat/')[1])
		})
	})
}

socket.on ('received_new_message', function (message) {
	var message_bubble = document.createElement ('p')
	var last_child = document.querySelector ('div#chat div:last-of-type')

	message_bubble.innerHTML = message

	if (last_child && last_child.className == 'user') {
		last_child = document.createElement ('div')
		last_child.className = 'target'
		last_child.appendChild (document.getElementById ('target_image').cloneNode ())
	}

	last_child.appendChild (message_bubble)
	chat.insertBefore (last_child, document.getElementById ('empty'))

	scrollToTop (document.getElementById ('chat'), 1000)
})

function scrollToTop(element, duration) {
	var scrollStep = (element.scrollHeight - element.offsetHeight - element.scrollTop) * 1.5 / 15
	var scrollInterval = setInterval (function () {
		if (element.scrollTop < element.scrollHeight - element.offsetHeight) {
			element.scrollTop += scrollStep
		}
		else
			clearInterval (scrollInterval)
	}, 15)
}
