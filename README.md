# Matcha

A Dating site.

To run server
```shell
npm install
node index.js
```
To setup
```bash
curl localhost:3000/setup
```

Admin:
- admin:admin

Users:
- Daenerys:pass
- John:pass
- Sansa:pass
- Cersei:pass
- Tyrion:pass
- Margaery:pass
- Gendry:pass
- Drogo:pass
- Ygrid:pass

![Alt text](https://gitlab.com/42_Projects/matcha/raw/master/.matcha.png)
