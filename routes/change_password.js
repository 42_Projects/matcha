'use strict'

var sql		= require ('../modules/sql')
var tools	= require ('../modules/tools')
var hash	= require ('password-hash')

function update_password (key, password, callback) {
	sql.set ('UPDATE `User` \
				 SET `Password` = ? \
			   WHERE `Password_key` = ?',
					  [password, key],
					  function (lastID, changes) {
		if (changes > 0) {
			sql.set ('UPDATE `User` \
						 SET `Password_key` = \'\' \
					   WHERE `Password_key` = ?',
							 [key],
							 function (lastID, changes) {
				if (callback)
					callback (changes > 0)
			})
		} else {
			if (callback)
				callback (false)
		}
	})
}

module.exports = function (router)
{
	router.route ('/change_password')
	.get (function (req, res) {
		var infos = req.query

		sql.get ('SELECT * \
					FROM `User` \
				   WHERE `Password_key` = ?', [infos['key']], function (rows) {
			req.session.password_key = infos['key']
			if (rows.length > 0) {
				res.render ('change_password', {
					title: 'Change Password',
					change_password: true
				})
			} else {
				res.render ('error', {
					title: 'Error',
					error: true,
					errors: 0b100000
				})
			}
		})
	})
	.post (function (req, res) {
		if (!req.body)
			return res.redirect ('/')

		var infos = req.body
		var password = /^.{4,50}$/
		if (!infos.password || !infos.password_verification || !password.test (infos.password) || infos.password != infos.password_verification) {
			res.render ('error', {
				title: 'Error',
				error: true,
				errors: 0b10000000
			})
		} else {
			infos.password = hash.generate (infos.password, {
				algorithm: 'whirlpool'
			})
			update_password (req.session.password_key, infos.password, function (updated) {
				if (updated) {
					res.redirect ('/')
				} else {
					res.render ('error', {
						title: 'Error',
						error: true,
						errors: 0b100000000
					})
				}
			})
		}
		req.session.password_key = ''
	})
}
