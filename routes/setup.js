'use strict'

var log		= require ('../modules/log')

module.exports = function (router)
{
	var setup = require ('../admin/setup')

	router.route ('/setup')
	.get (function (req, res) {
		log ('database', 'Installing database...')
		setup.install_database()
		res.end ('Installing database...')
	})
}
