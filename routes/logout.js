'use strict'

module.exports = function (router)
{
	router.route ('/logout')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0)
			return res.redirect ('/')
		req.session.destroy ()
		res.redirect ('/')
	})
}
