'use strict'

var sql		= require ('../modules/sql')
var tools	= require ('../modules/tools')
var mail	= require ('../modules/mail')
var random	= require ('randomstring')

module.exports = function (router)
{
	router.route ('/lost_password')
	.get (function (req, res) {
		res.render ('lost_password', {
			title: `Lost Password`,
			lost_password: true
		})
	})
	.post (function (req, res) {
		if (!req.body)
			return res.redirect ('/')

		var infos = req.body
		infos.mail = unescape (infos.mail)
		var mail_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
		if (mail_regex.test (infos.mail)) {
			sql.get ('SELECT `Mail` FROM `User` WHERE `Mail` = ?', [infos.mail], function (rows) {
				if (rows.length > 0) {
					var key = random.generate ()
					sql.set ('UPDATE `User` SET `Password_key` = ? WHERE `Mail` = ?', [key, infos.mail], function (lastID, changes) {
						if (changes > 0) {
							var link = req.protocol + '://' + req.get ('host') + '/change_password?key=' + key
							mail.send (infos.mail, 'Reset password', '<a href = "' + link + '"><input type = "button" value = "I want to change my password." style = "color: white; font-weight: bold; padding: 10px; background-color: #C3207F; border: none; border-radius: 5px">')
						}
						res.redirect ('/')
					})
				} else {
					res.render ('error', {
						title: 'Error',
						error: true,
						errors: 0b1000000
					})
				}
			})
		} else {
			res.redirect ('/lost_password')
		}
	})
}
