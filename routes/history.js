'use strict'

var sql		= require ('../modules/sql')

module.exports = function (router)
{
	router.route (/^\/history\/[a-zA-Z]+$/)
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0) return res.redirect ('/')

		var count = ''
		switch (req.url.split ('/history/')[1]) {
			case 'like':
				break;
			case 'likePink':
				req.session.user_infos.likePink_history_count = 0
				break;
			case 'visit':
				break;
			case 'visitPink':
				req.session.user_infos.visitPink_history_count = 0
				break;
			default:
				res.redirect ('/')
		}

		sql.set ('UPDATE `User` \
					 SET `' + req.url.split ('/history/')[1] + '_history_count` = 0 \
				   WHERE `User_id` = ?',
				   [req.session.user_id],
				   function (lastID, changes) {})

		return res.render ('history', {
			title: 'History',
			session: req.session,
			badges_history: [
				req.session.user_infos.visitPink_history_count,
				req.session.user_infos.likePink_history_count
			],
			badges: [
				req.session.user_infos.visitPink_history_count + req.session.user_infos.likePink_history_count,
				req.session.user_infos.messages_count
			],
			tags: []
		})
	})
}
