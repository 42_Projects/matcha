'use strict'

module.exports = function (router)
{
	router.route ('/')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0)
			return res.redirect ('/')
	})
	// .post (function (req, res) {
	// 	if (!req.session.user_id || req.session.user_id <= 0)
	// 		return res.redirect ('/')
	// })
}
