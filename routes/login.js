'use strict'

var hash	= require ('password-hash')
var sql		= require ('../modules/sql')
var tools	= require ('../modules/tools')
var moment	= require ('moment')

function check_values_connection (infos, callback) {
	var mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
	var pseudo = /^[a-zA-Z0-9_-]{3,15}$/
	var password = /^.{4,50}$/
	var errors = 0b00000

	if (!infos.pseudo || (!pseudo.test (infos.pseudo && !mail.test (infos.pseudo))))
		errors |= 0b00001
	if (!infos.password || !password.test (infos.password))
		errors |= 0b00010
	if (!errors) {
		sql.get ('SELECT * \
					FROM `User` \
			  INNER JOIN `Pictures` \
					  ON `User`.`User_id` = `Pictures`.`User_id` \
			  INNER JOIN `Colors` \
					  ON `User`.`User_id` = `Colors`.`User_id` \
				   WHERE (`Mail` = ? OR `Pseudo` = ?)',
				[unescape (infos.pseudo), infos.pseudo], function (rows) {
			if (rows && rows[0]) {
				if (rows[0].key != 'validated')
					errors |= 0b00100
				if (!hash.verify (infos.password, rows[0].password))
					errors |= 0b01000
				if (callback)
					callback (errors, rows[0])
			} else {
				if (callback)
					callback (errors | 0b10000, null)
			}
		})
	}
	else if (callback)
		callback (errors, null)
}

module.exports = function (router)
{
	router.route ('/login')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0)
			return res.redirect ('/')
	})
	.post (function (req, res) {
		if (!req.body)
			return res.redirect ('/')

		var flag = 0
		var infos = req.body
		if (infos.submit == 'Forgot+password') {
			res.redirect ('/lost_password')
			return
		}
		check_values_connection (infos, function (errors, row) {
			if (!errors) {
				var need_to_visit_profil = row.settings == -1

				if (need_to_visit_profil)
					sql.set ('UPDATE `User` \
								 SET `Settings` = 0 \
							   WHERE `User_id` = ?',
							   [row.user_id],
							   function () {})
				sql.set ('UPDATE `User` \
							 SET `Last_sign_of_life` = ? \
						   WHERE `User_id` = ?',
						   [new Date ().getTime (), row.user_id],
						   function (lastID, changes) {})
				sql.get ('SELECT `Title` \
							FROM `Tag` \
						   WHERE `User_id` = ?',
						   [row.user_id],
						   function (tags) {
					sql.get ('SELECT * \
								FROM `Chat` \
							   WHERE `User_id` = ? \
								  OR `Target_id` = ?',
								  [row.user_id, row.user_id],
								  function (chat) {
						row.messages_count = 0
						for (var c in chat) {
							row.messages_count += chat[c].user_id == row.user_id ? chat[c].user_unread : chat[c].target_unread
						}
						row.age = Math.floor ((new Date - new Date (row.birthday)) / 1000 / (60 * 60 * 24) / 365.25)
						req.session.user_id = row.user_id
						req.session.user_infos = row
						for (var tag in tags) {
							if (tags.hasOwnProperty(tag)) {
								tags[tag] = tags[tag].title
							}
						}
						req.session.tags = tags
						res.redirect (need_to_visit_profil ? '/profil' : '/')
					})
				})
			} else {
				req.session.errors = errors
				res.redirect ('/error')
			}
		})
	})
}
