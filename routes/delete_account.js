'use strict'

var sql		= require ('../modules/sql')
var log		= require ('../modules/log')

module.exports = function (router)
{
	router.route ('/delete_account')
	.post (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0)
			return res.redirect ('/')

		if (req.body && req.body.be_sure === 'true') {
			sql.set ('DELETE \
						FROM `User` \
					   WHERE `User_id` = ?',
							 [req.session.user_id],
							 function () {})
			sql.set ('DELETE \
						FROM `Pictures` \
					   WHERE `User_id` = ?',
							 [req.session.user_id],
							 function () {})
			sql.set ('DELETE \
						FROM `Tag` \
					   WHERE `User_id` = ?',
							 [req.session.user_id],
							 function () {})
			sql.set ('DELETE \
						FROM `Like` \
					   WHERE `User_id` = ?',
							 [req.session.user_id],
							 function () {})
			sql.set ('DELETE \
						FROM `Visit` \
					   WHERE `User_id` = ?',
							 [req.session.user_id],
							 function () {})
			log ('users', 'User no ' + req.session.user_id + ' deleted his account.')
			req.session.user_id = 0
			res.redirect ('/')
		} else
			res.redirect ('/profil')
	})
}
