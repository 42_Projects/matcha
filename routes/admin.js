'use strict'

var sql		= require ('../modules/sql')

module.exports = function (router)
{
	router.route ('/admin')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0 || req.session.user_infos.settings <= 0)
			return res.redirect ('/')
		res.redirect ('/admin/list_of_users')
	})

	router.route ('/admin/list_of_users')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0 || req.session.user_infos.settings <= 0)
			return res.redirect ('/')

		sql.get ('SELECT * \
					FROM `User` \
			  INNER JOIN `Pictures` \
					  ON `User`.`User_id` = `Pictures`.`User_id`',
						 [],
						 function (rows) {
			res.render ('ADMIN_list_of_users', {
				title: 'Admin',
				users: rows,
				session: req.session,
				badges_history: [
					req.session.user_infos.visitPink_history_count,
					req.session.user_infos.likePink_history_count
				],
				badges: [
					req.session.user_infos.visitPink_history_count + req.session.user_infos.likePink_history_count,
					req.session.user_infos.messages_count
				]
			})
		})
	})
}
