'use strict'

var hash	= require ('password-hash')
var sql		= require ('../modules/sql')
var tools	= require ('../modules/tools')
var mail	= require ('../modules/mail')
var random	= require ('randomstring')

function check_values_registration (infos, callback) {
	var mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
	var pseudo = /^[a-zA-Z0-9_-]{3,15}$/
	var password = /^.{4,50}$/
	var name = /^([A-Z]?[a-z]{2,15}-?[A-Z]?[a-z]{2,15})$|^([A-Z]?[a-z]{2,34})$/
	var flag = 0
	var check_pseudo = function (flag, pseudo, callback) {
		sql.get ('SELECT `Pseudo` \
					FROM `User` \
				   WHERE `Pseudo` = ?',
						 [pseudo],
						 function (rows) {
			if (rows && rows.length > 0)
				flag |= 0b01000000
			if (callback)
				callback (flag)
		})
	}
	var check_mail = function (flag, mail, callback) {
		sql.get ('SELECT `Mail` FROM `User` WHERE `Mail` = ?', [unescape (mail)], function (rows) {
			if (rows && rows.length > 0)
				flag |= 0b10000000
			if (callback)
				callback (flag)
		})
	}

	infos.mail = unescape (infos.mail)
	if (!infos.pseudo || !pseudo.test (infos.pseudo))
		flag |= 0b00000001
	if (!infos.gender || infos.gender < 1 || infos.gender > 2)
		flag |= 0b00000010
	if (!infos.mail || !mail.test (infos.mail))
		flag |= 0b00000100
	if (!infos.firstname || !name.test (infos.firstname))
		flag |= 0b00001000
	if (!infos.lastname || !name.test (infos.lastname))
		flag |= 0b00010000
	if (!/^[0-9]{4}(-[0-9]{2}){2}$/.test (infos.birthday) || (new Date - new Date (infos.birthday)) / 1000 / (60 * 60 * 24) / 365.25 < 18)
		flag |= 0b00100000
	if (!infos.password || !infos.password_verification  || !password.test (infos.password) || infos.password != infos.password_verification)
		flag |= 0b01000000
	check_pseudo (flag, infos.pseudo, function (flag) {
		check_mail (flag, infos.mail, function (flag) {
			if (callback)
				callback (flag)
		})
	})
}

module.exports = function (router)
{
	router.route ('/registration')
	.get (function (req, res) {
		sql.set ('UPDATE `User` SET `Key` = \'validated\' WHERE `Key` = ?', [req.query['key']], function (lastID, changes) {
			res.redirect ('/')
		})
	})
	.post (function (req, res) {
		if (!req.body)
			return res.redirect ('/')

		var flag = 0
		var infos = req.body
		check_values_registration (infos, function (flag) {
			if (!flag) {
				res.redirect ('/')
				infos.password = hash.generate (infos.password, {algorithm: 'whirlpool'})
				infos.key = random.generate ()
				infos.msg_key = random.generate ()
				infos.like_key = random.generate ()
				infos.visit_key = random.generate ()
				infos.profil_path = random.generate ()
				infos.images_path = random.generate ()
				sql.set ('INSERT INTO `User` (`Pseudo`, `Mail`, `Firstname`, `Lastname`, `Password`, `Key`, `Msg_key`, `Like_key`, `Visit_key`, `Profil_path`, `Images_path`, `Gender`, `Sexual_orientation`, `Birthday`, `Creation`) \
							VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [infos.pseudo, infos.mail, infos.firstname, infos.lastname, infos.password, infos.key, infos.msg_key, infos.like_key, infos.visit_key, infos.profil_path, infos.images_path, infos.gender, 2, infos.birthday, new Date ().getTime ()], function (lastID, changes) {
					sql.set ('INSERT INTO `Pictures` (`User_id`) VALUES (?)', [lastID], function () {})
					sql.set ('INSERT INTO `Colors` (`User_id`) VALUES (?)', [lastID], function () {})
					var link = req.protocol + '://' + req.get ('host') + '/registration?key=' + infos.key
					mail.send (infos.mail, 'Confirm your inscription to Matcha !', '<a href = "' + link + '"><input type = "button" value = "I confirm this mail address." style = "color: white; font-weight: bold; padding: 10px; background-color: #C3207F; border: none; border-radius: 5px">')
				})
			} else {
				req.session.flag = flag
				req.session.infos = tools.array_to_string (infos)
				res.redirect ('/')
			}
		})
	})
}
