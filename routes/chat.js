'use strict'

var sql		= require ('../modules/sql')
var jf		= require ('jsonfile')
var fs		= require ('fs')
var random	= require ('randomstring')
var moment	= require ('moment')

module.exports = function (router, io)
{
	router.route (/^\/chat\/[a-zA-Z0-9]{32}/)
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0)
			return res.redirect ('/')

		sql.get ('SELECT * \
					FROM `Chat` \
				   WHERE `File_path` = ?',
						 [req.url.split ('/chat/')[1]],
						 function (chat) {
			if (chat.length <= 0 || (chat[0].user_id != req.session.user_id && chat[0].target_id != req.session.user_id))
				return res.redirect ('/')

			var to_update = req.session.user_id == chat[0].user_id ? 'User_unread' : 'Target_unread'
			var last_value = req.session.user_id == chat[0].user_id ? chat[0].user_unread : chat[0].target_unread
			sql.set ('UPDATE `Chat` \
						 SET `' + to_update + '` = 0 \
					   WHERE `File_path` = ?',
							 [req.url.split ('/chat/')[1]],
							 function () {})
			req.session.user_infos.messages_count -= last_value

			chat[0].target_id = chat[0].target_id == req.session.user_id ? chat[0].user_id : chat[0].target_id
			sql.get ('SELECT * \
						FROM `Like` \
					   WHERE (`User_id` = ? AND `Target_id` = ?) \
						  OR (`User_id` = ? AND `Target_id` = ?)',
							 [req.session.user_id, chat[0].target_id, chat[0].target_id, req.session.user_id],
							 function (likes) {
				if (likes.length < 2) return res.redirect ('/')

				const path = require('path').dirname(require.main.filename)
				jf.readFile (path + '/resources/chat/' + chat[0].file_path, function (err, obj) {
					if (err) {
						jf.writeFile (path + '/resources/chat/' + chat[0].file_path, {'messages': []}, (err) => {})
						obj = {'messages': []}
					}

					sql.get ('SELECT `User`.`Profil_path`, \
									 `User`.`Images_path`, \
									 `Pictures`.`Image_0` \
								FROM `User` INNER JOIN `Pictures` \
								  ON `User`.`User_id` = `Pictures`.`User_id` \
							   WHERE `User`.`User_id` = ?',
									 [chat[0].target_id],
									 function (target) {
						if (!target || !target[0]) return res.redirect ('/chat')

						var msgs = {'messages': []}
						var array = []
						var last_autor = -1
						var current_autor = -1

						for (var i in obj.messages) {
							// Si on repasse a l'autre utilisateur
							if (last_autor == -1 || obj.messages[i].autor != last_autor) {
								if (i > 0) {
									msgs.messages.push ({
										autor: last_autor,
										msgs: array
									})
								}
								last_autor = obj.messages[i].autor
								array = []
							}

							array.push ({
								msg: obj.messages[i].message,
								date: moment (obj.messages[i].date).format ('L LTS')
							})
							current_autor = obj.messages[i].autor
						}
						if (last_autor >= 0) {
							msgs.messages.push ({
								autor: current_autor,
								msgs: array
							})
						}

						res.render ('chatting', {
							title: 'Chat',
							session: req.session,
							badges_history: [
								req.session.user_infos.visitPink_history_count,
								req.session.user_infos.likePink_history_count
							],
							badges: [
								req.session.user_infos.visitPink_history_count + req.session.user_infos.likePink_history_count,
								req.session.user_infos.messages_count
							],
							messages: msgs.messages,
							target: target[0]
						})
					})
				})
			})
		})
	})

	router.route ('/chat')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0)
			return res.redirect ('/')

		var gender = {
			man: 1,
			woman: 2
		}
		var sexual_orientation = {
			homo: 1,
			hetero: 2,
			bi: 3
		}

		var user_gender = req.session.user_infos.gender
		var user_sexual_orientation = req.session.user_infos.sexual_orientation
		var looked_gender
		var looked_sexual_orientation
		var bi_condition = ''

		// Si homo
		if (user_sexual_orientation == sexual_orientation.homo) {
			looked_gender = [user_gender == gender.man ? gender.man : gender.woman]
			looked_sexual_orientation = [sexual_orientation.homo, sexual_orientation.bi]
		}

		if (user_sexual_orientation == sexual_orientation.hetero) {
			looked_gender = [user_gender == gender.man ? gender.woman : gender.man]
			looked_sexual_orientation = [sexual_orientation.hetero, sexual_orientation.bi]
		}

		if (user_sexual_orientation == sexual_orientation.bi) {
			looked_gender = [gender.man, gender.woman]
			looked_sexual_orientation = [sexual_orientation.homo, sexual_orientation.hetero, sexual_orientation.bi]
			bi_condition = ' AND NOT (`User`.`Gender` == ' + user_gender + ' AND `User`.`Sexual_orientation` == ' + sexual_orientation.hetero + ')'
		}

		looked_gender = looked_gender.toString ()
		looked_sexual_orientation = looked_sexual_orientation.toString ()

		var args = [req.session.user_id]
		var condition = '1'

		require ('../modules/get_list_of_users') (req.session.user_infos, '\
		SELECT `User`.`User_id`, \
			   `User`.`Firstname`, \
			   `User`.`Lastname`, \
			   `User`.`Pseudo`, \
			   `User`.`Birthday`, \
			   `User`.`Bio`, \
			   `User`.`Score`, \
			   `User`.`City`, \
			   `User`.`Like_key`, \
			   `User`.`Visit_key`, \
			   `User`.`Profil_path`, \
			   `User`.`Images_path`, \
			   `User`.`Gender`, \
			   `User`.`Sexual_orientation`, \
			   `User`.`Last_sign_of_life`, \
			   `User`.`Longitude`, \
			   `User`.`Latitude`, \
			   `Pictures`.`Image_0`, \
			   `Chat`.`File_path`, \
			   `Chat`.`User_id`, \
			   `Chat`.`User_unread`, \
			   `Chat`.`Target_unread` \
		  FROM `User` \
	INNER JOIN `Pictures` \
			ON `User`.`User_id` = `Pictures`.`User_id` \
	 LEFT JOIN `Like` \
			ON `User`.`User_id` = `Like`.`User_id` \
			OR `User`.`User_id` = `Like`.`Target_id` \
	 LEFT JOIN `Visit` \
			ON `User`.`User_id` = `Visit`.`User_id` \
			OR `User`.`User_id` = `Visit`.`Target_id` \
	 LEFT JOIN `Tag` \
			ON `User`.`User_id` = `Tag`.`User_id` \
	 LEFT JOIN `Block` \
			ON `User`.`User_id` = `Block`.`Target_id` \
	INNER JOIN `Chat` \
			ON (`User`.`User_id` = `Chat`.`Target_id` \
		   AND `Chat`.`User_id` = ' + req.session.user_id + ') \
			OR (`User`.`User_id` = `Chat`.`User_id` \
		   AND `Chat`.`target_id` = ' + req.session.user_id + ') \
		 WHERE ' + condition + ' \
		   AND `User`.`User_id` != ? \
		   AND `User`.`Gender` \
			IN (' + looked_gender + ') \
		   AND `User`.`Sexual_orientation` \
			IN (' + looked_sexual_orientation + ') ' + bi_condition + ' \
		   AND `Block`.`Target_id` IS NULL \
	  GROUP BY `User`.`User_id`',
	  			args,
				function (users) {
			res.render ('chat', {
				title: 'Chat',
				session: req.session,
				badges_history: [
					req.session.user_infos.visitPink_history_count,
					req.session.user_infos.likePink_history_count
				],
				badges: [
					req.session.user_infos.visitPink_history_count + req.session.user_infos.likePink_history_count,
					req.session.user_infos.messages_count
				],
				users: users
			})
		})
	})
}
