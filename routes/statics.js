'use strict'

module.exports = function (router)
{
	router.route (['*/js/*'])
	.get (function (req, res) {
		var path = req.originalUrl.split ('js/')[1]
		var sub_path = '/js/'
		res.sendFile (process.env.PWD + sub_path + path)
	})

	router.route (['*/css/*'])
	.get (function (req, res) {
		var path = req.originalUrl.split ('css/')[1]
		var sub_path = '/css/'
		res.sendFile (process.env.PWD + sub_path + path)
	})

	router.route (['*/resources/*'])
	.get (function (req, res) {
		var path = req.originalUrl.split ('resources/')[1]
		var sub_path = '/resources/'
		res.sendFile (process.env.PWD + sub_path + path)
	})
}
