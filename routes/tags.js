'use strict'

var sql		= require ('../modules/sql')

module.exports = function (router)
{
	router.route (/^\/tags\/[a-zA-Z0-9]{2,10}\/?$/)
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0) return res.redirect ('/')

		sql.get ('SELECT `Title` \
					FROM `Tags` \
				   WHERE `Uses` > 0 \
				ORDER BY `Uses` DESC', function (tags) {
			return res.render ('matches', {
				title: 'Matcha',
				session: req.session,
				badges_history: [
					req.session.user_infos.visitPink_history_count,
					req.session.user_infos.likePink_history_count
				],
				badges: [
					req.session.user_infos.visitPink_history_count + req.session.user_infos.likePink_history_count,
					req.session.user_infos.messages_count
				],
				tags: []
			})
		})
	})
}
