'use strict'

module.exports = function (router)
{
	router.route ('/*')
	.get (function (req, res) {
		res.end ('404 Not found.')
	})
	.post (function (req, res) {
		res.end ('404 Not found.')
	})
}
