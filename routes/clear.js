'use strict'

var setup	= require ('../admin/setup')
var log		= require ('../modules/log')

module.exports = function (router)
{
	router.route ('/clear')
	.get (function (req, res) {
		log ('database', 'Clearing database...')
		setup.clear_database()
		res.end ('Clearing database...')
	})
}
