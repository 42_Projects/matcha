'use strict'

var sql				= require ('../modules/sql')
var config			= require ('../config/config')
var fse				= require ('fs-extra')
var hash			= require ('password-hash')
var XMLHttpRequest	= require ('xmlhttprequest').XMLHttpRequest
var fs				= require ('fs')
var random			= require ('randomstring')
var jf				= require ('jsonfile')
var moment			= require ('moment')

function get_distance (user_lat, user_lng, target_lat, target_lng) {
	const PI = Math.PI

	var dist_lat = (target_lat - user_lat) * (Math.PI / 180)
	var dist_lng = (target_lng - user_lng) * (Math.PI / 180)
	var d = Math.pow (Math.sin (dist_lat / 2.0), 2) + Math.cos (user_lat * (Math.PI / 180)) * Math.cos (target_lat * (Math.PI / 180)) * Math.pow (Math.sin (dist_lng / 2.0), 2)
	d = 2 * Math.atan (Math.sqrt (d), Math.sqrt (1 - d))

	return 6367 * d
}

module.exports = function (router)
{
	router.route ('/http_request/set_location')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0)
			return res.end ()
		var lat = parseFloat (req.query.lat)
		var lng = parseFloat (req.query.lng)
		if (lat < -60)	lat = -60
		if (lat > 84)	lat = 84

		var xhr = new XMLHttpRequest ()

		xhr.onreadystatechange = function () {
			if (this.readyState === 4) {
				var city = JSON.parse (this.responseText).results[0].formatted_address.split (', ')[1].split (' ')

				if (city[1] == 'Paris')
					city = 'Paris, ' + city[0].split ('0')[1] + 'ème'
				else
					city = city[1]
				sql.set ('UPDATE `User` SET `Latitude` = ?, `Longitude` = ?, `City` = ? WHERE `User_id` = ?', [lat, lng, city, req.session.user_id], function (lastID, changes) {
					res.end (changes > 0 ? 'true' : 'false')
				})
			}
		}

		xhr.open ('GET', 'https://maps.googleapis.com/maps/api/geocode/json?key=' + config.KEY_map + '&latlng=' + lat + ',' + lng)
		xhr.send ()
	})

	router.route ('/http_request/get_location')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0)
			return res.end ()
		sql.get ('SELECT `Latitude`, \
						 `Longitude` \
					FROM `User` \
				   WHERE `User_id` = ?',
						 [req.session.user_id],
						 function (rows) {
			if (rows && rows[0])
				res.end (rows[0].latitude + ';' + rows[0].longitude)
			else
				res.end (null)
		})
	})

	router.route ('/http_request/get_location_of')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0) return res.end ()

		var visit_key = req.headers.referer.split ('/')
		visit_key = visit_key[visit_key.length - 1]
		if (!/^[a-zA-Z0-9]{32}$/.test (visit_key)) return res.end ()
		sql.get ('SELECT `User_id` \
					FROM `User` \
				   WHERE `Visit_key` = ?',
						 [visit_key],
						 function (user_id) {
			if (user_id.length <= 0) return res.end ()
			user_id = user_id[0].user_id
			sql.get ('SELECT `Latitude`, \
							 `Longitude` \
						FROM `User` \
					   WHERE `User_id` = ?',
							 [user_id],
							 function (rows) {
				if (rows && rows[0])
					res.end (rows[0].latitude + ';' + rows[0].longitude)
				else
					res.end (null)
			})
		})
	})

	router.route ('/http_request/report')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0) return res.end ()

		var visit_key = req.headers.referer.split ('/')
		visit_key = visit_key[visit_key.length - 1]
		if (!/^[a-zA-Z0-9]{32}$/.test (visit_key)) return res.end ()
		sql.get ('SELECT `User_id` FROM `User` WHERE `Visit_key` = ?', [visit_key], function (target_id) {
			if (target_id.length <= 0) return res.end ()
			target_id = target_id[0].user_id
			sql.get ('SELECT `Report_id` FROM `Report` WHERE `User_id` = ? AND `Target_id` = ?', [req.session.user_id, target_id], function (rows) {
				if (rows && rows[0])
					return res.end ()
				sql.set ('INSERT INTO `Report` (`User_id`, `Target_id`, `Creation`) VALUES (?, ?, ?)', [req.session.user_id, target_id, new Date ().getTime ()], function (lastID, changes) {})
				sql.set ('UPDATE `User` SET `Report` = `Report` + 1 WHERE `User_id` = ?', [target_id], function (lastID, changes) {})
				return res.end ('true')
			})
		})
	})

	router.route ('/http_request/block')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0) return res.end ()

		var visit_key = req.headers.referer.split ('/')
		visit_key = visit_key[visit_key.length - 1]
		if (!/^[a-zA-Z0-9]{32}$/.test (visit_key)) return res.end ()
		sql.get ('SELECT `User_id` FROM `User` WHERE `Visit_key` = ?', [visit_key], function (target_id) {
			if (target_id.length <= 0) return res.end ()
			target_id = target_id[0].user_id
			sql.get ('SELECT `Block_id` FROM `Block` WHERE `User_id` = ? AND `Target_id` = ?', [req.session.user_id, target_id], function (rows) {
				if (rows && rows[0])
					return res.end ()
				sql.set ('INSERT INTO `Block` (`User_id`, `Target_id`, `Creation`) VALUES (?, ?, ?)', [req.session.user_id, target_id, new Date ().getTime ()], function (lastID, changes) {})
				return res.end ('true')
			})
		})
	})

	router.route ('/http_request/delete_image')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0)
			return res.end ()

		var image_id = req.query.id
		if (image_id < 0 || image_id > 3) return
		sql.set ('UPDATE `Pictures` SET `Image_' + image_id + '` = ? WHERE `User_id` = ?', [false, req.session.user_id], function (lastID, changes) {
			if (image_id == '0')
				req.session.user_infos.image_0 = false
			else if (image_id == '1')
				req.session.user_infos.image_1 = false
			else if (image_id == '2')
				req.session.user_infos.image_2 = false
			else if (image_id == '3')
				req.session.user_infos.image_3 = false
			req.session.user_infos.photos_count--
			req.session.save (function (err) {
				if (err)
					return console.error (err)
				fse.remove ('resources/profils/' + req.session.user_id + '/image_' + image_id, function (err) {
					res.end ('done')
				})
			})
		})
	})

	router.route ('/http_request/validate_user')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0 || req.session.user_infos.settings == 0)
			return res.end (null)
		sql.set ('UPDATE `User` SET `key` = \'validated\' WHERE `User_id` = ?', [req.query.user_id], function (lastID, changes) {
			if (changes > 0)
				res.end ('done')
			else
				res.end (null)
		})
	})

	router.route ('/http_request/delete_account')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0 || req.session.user_infos.settings == 0)
			return res.end (null)

		sql.set ('DELETE FROM `User` WHERE `User_id` = ?', [req.query.user_id], null)
		sql.set ('DELETE FROM `Pictures` WHERE `User_id` = ?', [req.query.user_id], null)
		sql.set ('DELETE FROM `Tag` WHERE `User_id` = ?', [req.query.user_id], null)
		sql.set ('DELETE FROM `Like` WHERE `User_id` = ?', [req.query.user_id], null)
		sql.set ('DELETE FROM `Visit` WHERE `User_id` = ?', [req.query.user_id], null)
		res.end ('done')
	})

	router.route ('/http_request/im_alive')
	.get (function (req, res) {
		if (!req.session.user_id) return

		sql.set ('UPDATE `User` SET `last_sign_of_life` = ? WHERE `User_id` = ?', [new Date ().getTime (), req.session.user_id], function () {})
	})

	router.route ('/http_request/like')
	.get (function (req, res) {
		if (!req.session.user_id || !req.query || req.session.user_infos.photos_count <= 0) return res.end ()

		var key = req.query.key.split (';')
		if (!hash.verify (key[0] + config.HASH_like + req.session.user_infos.like_key, key[1]))
			return res.end ()

		sql.get ('SELECT `User_id`, `Socket_id` FROM `User` WHERE `Like_key` = ?', [key[0]], function (rows) {
			if (rows.length <= 0) return res.end ()
			sql.get ('SELECT * FROM `Like` WHERE `User_id` = ? AND `Target_id` = ? OR `User_id` = ? AND `Target_id` = ?', [req.session.user_id, rows[0].user_id, rows[0].user_id, req.session.user_id], function (like) {
				if (like.length == 2 || (like.length == 1 && like[0].user_id == req.session.user_id)) {
					sql.set ('DELETE FROM `Like` WHERE `Like_id` = ?', [like[0].like_id], function () {})
					sql.set ('UPDATE `Chat` SET `Enabled` = 0 WHERE `User_id` = ? AND `Target_id` = ? OR `User_id` = ? AND `Target_id` = ?', [req.session.user_id, rows[0].user_id, rows[0].user_id, req.session.user_id], function () {})
					sql.set ('UPDATE `User` SET `Score` = `Score` - ?, `LikePink_history_count` = `LikePink_history_count` - 1 WHERE `User_id` = ?', [config.POINTS_like, rows[0].user_id], function () {})
				} else {
					sql.set ('INSERT INTO `Like` (`User_id`, `Target_id`, `Creation`) VALUES (?, ?, ?)', [req.session.user_id, rows[0].user_id, new Date ().getTime ()], function (lastID, changes) {
						if (changes <= 0) return res.end ()
						sql.set ('UPDATE `User` SET `Score` = `Score` + ?, `LikePink_history_count` = `LikePink_history_count` + 1 WHERE `User_id` = ?', [config.POINTS_like, rows[0].user_id], function () {})

						if (like.length == 1) {
							sql.get ('SELECT * FROM `Chat` WHERE `User_id` = ? AND `Target_id` = ? OR `User_id` = ? AND `Target_id` = ?', [req.session.user_id, rows[0].user_id, rows[0].user_id, req.session.user_id], function (chat) {
								if (chat.length == 0) {
									var file_path = random.generate ()
									sql.set ('INSERT INTO `Chat` (`User_id`, `Target_id`, `Creation`, `File_path`) VALUES (?, ?, ?, ?)', [req.session.user_id, rows[0].user_id, new Date ().getTime (), file_path], function (lastID, changes) {
										jf.writeFile (require('path').dirname(require.main.filename) + '/resources/chat/' + file_path, {'messages': []}, (err) => {})
									})
								} else {
									sql.set ('UPDATE `Chat` SET `Enabled` = 1 WHERE `User_id` = ? AND `Target_id` = ? OR `User_id` = ? AND `Target_id` = ?', [req.session.user_id, rows[0].user_id, rows[0].user_id, req.session.user_id], function () {})
								}
							})
						}
					})
					if (like.length == 1)
						return res.end ('2;' + rows[0].socket_id)
				}
				return res.end ((like.length == 0) + ';' + rows[0].socket_id)
			})
		})
	})

	router.route ('/http_request/update_likePink_count')
	.get (function (req, res) {
		if (!req.session.user_id || !req.query) return res.end ()

		req.session.user_infos.likePink_history_count += parseInt (req.query.update)
		res.end ('done')
	})

	router.route ('/http_request/update_visitPink_count')
	.get (function (req, res) {
		if (!req.session.user_id || !req.query) return res.end ()

		req.session.user_infos.visitPink_history_count += parseInt (req.query.update)
		res.end ('done')
	})

	router.route ('/http_request/update_messages_count')
	.get (function (req, res) {
		if (!req.session.user_id || !req.query) return res.end ()

		sql.get ('SELECT * FROM `Chat` WHERE `File_path` = ?', [req.query.file_path], function (chat) {
			if (!chat || chat.length == 0) return

			var to_update = req.session.user_id == chat[0].user_id ? 'User_unread' : 'Target_unread'
			sql.set ('UPDATE `Chat` SET `' + to_update + '` = `' + to_update + '` + 1 WHERE `File_path` = ?', [req.query.file_path], function () {})
			req.session.user_infos.messages_count += parseInt (req.query.update)
			res.end ('done')
		})
	})

	router.route ('/http_request/get_matches')
	.get (function (req, res) {
		if (!req.session.user_id || !req.query) return res.end ()

		req.query.min_age = parseInt (req.query.min_age && !isNaN (req.query.min_age) && req.query.min_age >= 18 ? req.query.min_age : 18)
		req.query.max_age = parseInt (req.query.max_age && !isNaN (req.query.max_age) ? req.query.max_age : 60)
		req.query.min_score = parseInt (req.query.min_score && !isNaN (req.query.min_score) && req.query.min_score >= 0 ? req.query.min_score : 0)
		req.query.max_score = parseInt (req.query.max_score && !isNaN (req.query.max_score) ? req.query.max_score : 100000)
		req.query.min_distance = parseInt (req.query.min_distance && !isNaN (req.query.min_distance && req.query.min_distance >= 0) ? req.query.min_distance : 0)
		req.query.max_distance = parseInt (req.query.max_distance && !isNaN (req.query.max_distance) ? req.query.max_distance : 21000)
		req.query.order_by = req.query.order_by ? req.query.order_by : 'points'

		if (req.query.tags && req.query.tags.length > 0) {
			req.query.tags = req.query.tags.split (/,\ ?/).filter (function (e) {
				return (/^#?[a-zA-Z0-9]{2,10}$/.exec (e))
			}).map (function (e) {
				return (/#/.exec (e) ? '' : '#') + e
			}).toString ()

			req.query.tags = req.query.tags.replace (/#[a-zA-Z0-9]+/g, '\"$&\"')
		} else {
			req.query.tags = ''
		}

		var d = new Date ()
		var limit = req.query.limit
		var offset = req.query.offset
		var max_birthday = (d.getFullYear () - parseInt (req.query.min_age)) + '' + moment ().format ('[/]MM[/]DD')
		var min_birthday = (d.getFullYear () - parseInt (req.query.max_age)) + '' + moment ().format ('[/]MM[/]DD')
		var args = [req.session.user_id, req.session.user_id]
		var condition = '1'

		switch (req.headers.referer.split ('/history/')[1]) {
			case 'like':
				condition = '`Like`.`User_id` = ?'
				break;
			case 'likePink':
				condition = '`Like`.`Target_id` = ?'
				break;
			case 'visit':
				condition = '`Visit`.`User_id` = ?'
				break;
			case 'visitPink':
				condition = '`Visit`.`Target_id` = ?'
				break;
			default:
				args.splice (0, 1)
		}


		if (req.headers.referer.indexOf ('tags') == 22) {
			condition = '`Tag`.`Title` = ?'
			args.unshift ('#' + req.headers.referer.split ('/tags/')[1])
		}

		var gender = {
			man: 1,
			woman: 2
		}
		var sexual_orientation = {
			homo: 1,
			hetero: 2,
			bi: 3
		}

		var user_gender = req.session.user_infos.gender
		var user_sexual_orientation = req.session.user_infos.sexual_orientation
		var looked_gender
		var looked_sexual_orientation
		var bi_condition = ''

		if (user_sexual_orientation == sexual_orientation.homo) {
			looked_gender = [user_gender == gender.man ? gender.man : gender.woman]
			looked_sexual_orientation = [sexual_orientation.homo, sexual_orientation.bi]
		}

		if (user_sexual_orientation == sexual_orientation.hetero) {
			looked_gender = [user_gender == gender.man ? gender.woman : gender.man]
			looked_sexual_orientation = [sexual_orientation.hetero]
		}

		if (user_sexual_orientation == sexual_orientation.bi) {
			looked_gender = [gender.man, gender.woman]
			looked_sexual_orientation = [sexual_orientation.homo, sexual_orientation.hetero, sexual_orientation.bi]
			bi_condition = ' AND NOT (`User`.`Gender` == ' + user_gender + ' AND `User`.`Sexual_orientation` == ' + sexual_orientation.hetero + ')'
		}

		looked_gender = looked_gender.toString ()
		looked_sexual_orientation = looked_sexual_orientation.toString ()

		require ('../modules/get_list_of_users') (req.session.user_infos, ' \
			SELECT `User`.`User_id`, \
				   `User`.`Firstname`, \
				   `User`.`Lastname`, \
				   `User`.`Pseudo`, \
				   `User`.`Birthday`, \
				   `User`.`Bio`, \
				   `User`.`Score`, \
				   `User`.`City`, \
				   `User`.`Like_key`, \
				   `User`.`Visit_key`, \
				   `User`.`Profil_path`, \
				   `User`.`Images_path`, \
				   `User`.`Gender`, \
				   `User`.`Sexual_orientation`, \
				   `User`.`Last_sign_of_life`, \
				   `User`.`Longitude`, \
				   `User`.`Latitude`, \
				   `User`.`Settings`, \
				   `Pictures`.`Image_0`, \
				   `Pictures`.`Image_1`, \
				   `Pictures`.`Image_2`, \
				   `Pictures`.`Image_3` \
			  FROM `User` \
		INNER JOIN `Pictures` \
				ON `User`.`User_id` = `Pictures`.`User_id` \
		 LEFT JOIN `Like` \
		 		ON `User`.`User_id` = `Like`.`User_id` \
				OR `User`.`User_id` = `Like`.`Target_id` \
		 LEFT JOIN `Visit` \
		 		ON `User`.`User_id` = `Visit`.`User_id` \
				OR `User`.`User_id` = `Visit`.`Target_id` \
		 LEFT JOIN `Tag` \
		 		ON `User`.`User_id` = `Tag`.`User_id` \
		 LEFT JOIN `Block` \
		 		ON `User`.`User_id` = `Block`.`Target_id` \
			 WHERE ' + condition + ' \
			   AND `User`.`User_id` != ? \
			   AND `User`.`Gender` IN (' + looked_gender + ') \
			   AND `User`.`Sexual_orientation` IN (' + looked_sexual_orientation + ') ' + bi_condition + ' \
			   AND `Block`.`Target_id` IS NULL \
			   AND `User`.`Birthday` >= \'' + min_birthday + '\' \
			   AND `User`.`Birthday` <= \'' + max_birthday + '\' \
			   AND `User`.`Score` >= \'' + req.query.min_score + '\' \
			   AND `User`.`Score` <= \'' + req.query.max_score + '\' \
			   AND (`Tag`.`Title` IN (' + req.query.tags + ')' + (req.query.tags && req.query.tags.length > 0 ? '' : ' OR 1 ') + ') \
		  GROUP BY `User`.`User_id`', args, function (users) {

			const pts_by_km = -5
			const pts_by_years = -5
			const pts_by_score = 3
			const pts_by_tags = 15

			for (var user in users) {
				var common_tags = 0

				users[user].distance = get_distance (req.session.user_infos.latitude, req.session.user_infos.longitude, users[user].latitude, users[user].longitude).toFixed (1)
				users[user].to_remove = users[user].settings < 0 || users[user].distance < req.query.min_distance || users[user].distance >= req.query.max_distance
				if (users[user].to_remove) continue
				for (var tag in req.session.tags) {
					common_tags += users[user].tags && users[user].tags.filter (function (e) {
						return e.title == req.session.tags[tag]
					}).length > 0
				}
				users[user].points = users[user].distance * pts_by_km +
									 Math.abs (users[user].age - req.session.user_infos.age) * pts_by_years +
									 users[user].score / 100 * pts_by_score +
									 common_tags * pts_by_tags
			}

			if (users) {
				users = users.filter (function (user) {
					return (!user.to_remove)
				})

				if (users) {
					users = users.slice (0).sort (function (a, b) {
						return (a[req.query.order_by] > b[req.query.order_by]) ? -1 : (a[req.query.order_by] < b[req.query.order_by]) ? 1 : 0
					})

					if (req.query.order_by_sens == 'ascending') {
						users.reverse ()
					}
				}
			}

			res.render ('elements', {
				need_more_users: users ? (parseInt(offset) + parseInt(limit)) < users.length : false,
				users: users ? users.splice (offset, limit) : [],
				session: req.session
			})
		})
	})

	router.route ('/http_request/add_message')
	.get (function (req, res) {
		if (!req.session.user_id || !req.query) return res.end ()

		var chat_path = req.headers.referer.split ('/chat/')[1]
		var obj = jf.readFileSync (require('path').dirname(require.main.filename) + '/resources/chat/' + chat_path)

		sql.get ('SELECT * FROM `Chat` WHERE `File_path` = ?', [chat_path], function (chat) {
			if (!chat || chat.length == 0) return res.end ()

			var target_id = chat[0].user_id == req.session.user_id ? chat[0].target_id : chat[0].user_id
			sql.get ('SELECT `User_id`, `Socket_id` FROM `User` WHERE `User_id` = ?', [target_id], function (target) {
				if (!target || target.length == 0) return res.end ()

				jf.readFile (require('path').dirname(require.main.filename) + '/resources/chat/' + chat_path, function (err, obj) {
					obj.messages.push ({
						autor: req.session.user_id,
						date: new Date ().getTime (),
						message: req.query.message
					})

					jf.writeFile (require ('path').dirname(require.main.filename) + '/resources/chat/' + chat_path, obj, function (err) {})
				})

				res.end (target[0].socket_id)
			})
		})
	})
}
