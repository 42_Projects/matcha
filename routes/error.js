'use strict'

module.exports = function (router)
{
	router.route ('/error')
	.get (function (req, res) {
		var errors = req.session.errors

		req.session.errors = null
		if (!errors)
			errors = 0
		res.render ('error', {
			title: 'Error',
			error: true,
			errors: errors ? errors : 0
		})
	})
}
