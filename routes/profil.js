'use strict'

var sql		= require ('../modules/sql')
var config	= require ('../config/config')
var fse		= require ('fs-extra')
var hash	= require ('password-hash')

function upload_file (file, user_id, path, image_id) {
	var new_path = 'resources/profils/' + path + '/'

	if (file.size > 0) {
		var tmp_path = file.path
		var file_name = 'image_' + image_id

		fse.copy (tmp_path, new_path + file_name, function (err) {})
		sql.set ('UPDATE `Pictures` SET `Image_' + image_id + '` = ? WHERE `User_id` = ?', [true, user_id], function (rows) {})
		return true
	}
	return false
}

module.exports = function (router, io)
{
	/* Visited profil */
	router.route (/^\/profil\/[a-zA-Z0-9]{32}\/?$/)
	.get (function (req, res) {
		if (!req.session.user_id) return res.redirect ('/')
		sql.get ('SELECT * FROM `User` INNER JOIN `Pictures` ON `User`.`User_id` = `Pictures`.`User_id` WHERE `Visit_key` = ?', [req.url.split ('/')[2]], function (rows) {
			if (rows.length <= 0) res.redirect ('/')

			sql.get ('SELECT * FROM `Visit` WHERE `User_id` = ? AND `Target_id` = ?', [req.session.user_id, rows[0].user_id], function (like) {
				if (like.length == 0) {
					sql.set ('INSERT INTO `Visit` (`User_id`, `Target_id`, `Creation`) VALUES (?, ?, ?)', [req.session.user_id, rows[0].user_id, new Date ().getTime ()], function (lastID, changes) {
						if (changes <= 0) return res.end ()
						sql.set ('UPDATE `User` SET `Score` = `Score` + ? WHERE `User_id` = ?', [config.POINTS_visit, rows[0].user_id], function () {})
						if (io.sockets.connected[rows[0].socket_id]) {
							io.sockets.connected[rows[0].socket_id].emit ('visit', req.session.user_infos.firstname)
						}
					})
				}
			})
			sql.get ('SELECT `Title` FROM `Tag` WHERE `User_id` = ?', [rows[0].user_id], function (tags) {
				res.render ('visited_profil', {
					title: 'Profil of ' + rows[0].firstname,
					badges_history: [
						req.session.user_infos.visitPink_history_count,
						req.session.user_infos.likePink_history_count
					],
					badges: [
						req.session.user_infos.visitPink_history_count + req.session.user_infos.likePink_history_count,
						req.session.user_infos.messages_count
					],
					session: req.session,
					infos: rows[0],
					tags: tags
				})
			})
		})
	})

	router.route ('/profil')
	.get (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0)
			return res.redirect ('/')

		res.render ('profil', {
			title: 'Profil',
			session: req.session,
			badges_history: [
				req.session.user_infos.visitPink_history_count,
				req.session.user_infos.likePink_history_count
			],
			badges: [
				req.session.user_infos.visitPink_history_count + req.session.user_infos.likePink_history_count,
				req.session.user_infos.messages_count
			]
		})
	})
	.post (function (req, res) {
		if (!req.session.user_id || req.session.user_id <= 0 || !req.body)
			return res.redirect ('/')

		var infos = req.body
		var flag = 0
		var tags = []
		var mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
		var pseudo = /^[a-zA-Z0-9_-]{3,15}$/
		var name = /^([A-Z]?[a-z]{2,15}-?[A-Z]?[a-z]{2,15})$|^([A-Z]?[a-z]{2,34})$/

		infos.mail = unescape (infos.mail)
		// if (!infos.pseudo || !pseudo.test (infos.pseudo))
		// 	flag |= 0b000000000001
		if (!infos.gender || infos.gender < 1 || infos.gender > 2)
			flag |= 0b000000000010
		if (!infos.mail || !mail.test (infos.mail))
			flag |= 0b000000000100
		if (!infos.firstname || !name.test (infos.firstname))
			flag |= 0b000000001000
		if (!infos.lastname || !name.test (infos.lastname))
			flag |= 0b000000010000
		if (!infos.password || !infos.password_verification  || !password.test (infos.password) || infos.password != infos.password_verification)
			flag |= 0b000001000000
		if (!infos.sexual_orientation || infos.sexual_orientation < 1 || infos.sexual_orientation > 3)
			flag |= 0b000010000000
		if (infos.bio.length > 300)
			flag |= 0b000100000000
		if (!infos.birthday || !/^[0-9]{4}(-[0-9]{2}){2}$/.test (infos.birthday) || (new Date - new Date (infos.birthday)) / 1000 / (60 * 60 * 24) / 365.25 < 18)
			flag |= 0b001000000000

		req.session.user_infos.image_0 |= upload_file (req.body.image_0, req.session.user_id, req.session.user_infos.profil_path + '/' + req.session.user_infos.images_path, 0)
		req.session.user_infos.image_1 |= upload_file (req.body.image_1, req.session.user_id, req.session.user_infos.profil_path + '/' + req.session.user_infos.images_path, 1)
		req.session.user_infos.image_2 |= upload_file (req.body.image_2, req.session.user_id, req.session.user_infos.profil_path + '/' + req.session.user_infos.images_path, 2)
		req.session.user_infos.image_3 |= upload_file (req.body.image_3, req.session.user_id, req.session.user_infos.profil_path + '/' + req.session.user_infos.images_path, 3)
		req.session.user_infos.photos_count = req.session.user_infos.image_0 + req.session.user_infos.image_1 + req.session.user_infos.image_2 + req.session.user_infos.image_3
		if (!flag || flag == 0b000001000000) {
			req.session.user_infos.mail = infos.mail
			req.session.user_infos.firstname = infos.firstname
			req.session.user_infos.lastname = infos.lastname
			req.session.user_infos.gender = infos.gender
			req.session.user_infos.birthday = infos.birthday
			req.session.user_infos.bio = infos.bio
			req.session.user_infos.sexual_orientation = infos.sexual_orientation
		}

		req.session.save (function () {})
		sql.set ('UPDATE `User` SET `Photos_count` = ? WHERE `User_id` = ?', [req.session.user_infos.photos_count, req.session.user_id], function (lastID, changes) {})

		if (!flag || flag == 0b000001000000) {
			sql.set ('UPDATE `User` SET `Gender` = ?, `Sexual_orientation` = ?, `Bio` = ?, `Birthday` = ?, `Photos_count` = ? WHERE `User_id` = ?', [infos.gender, infos.sexual_orientation, infos.bio, infos.birthday, req.session.user_infos.photos_count, req.session.user_id], function (lastID, changes) {})
			if (req.body.password.length > 0) {
				req.body.password = hash.generate (req.body.password, {algorithm: 'whirlpool'})
				sql.set ('UPDATE `User` SET `Mail` = ?, `Firstname` = ?, `Lastname` = ?, `Password` = ? WHERE `User_id` = ?', [infos.mail, infos.firstname, infos.lastname, infos.password, req.session.user_id], function (lastID, changes) {})
			} else {
				sql.set ('UPDATE `User` SET `Mail` = ?, `Firstname` = ?, `Lastname` = ? WHERE `User_id` = ?', [infos.mail, infos.firstname, infos.lastname, req.session.user_id], function (lastID, changes) {})
			}

			if (!Array.isArray (req.body.tags))
				req.body.tags = [req.body.tags]
			for (var tag in req.session.tags) {
				var to_delete = true
				for (var tag_ in req.body.tags) {
					if (req.session.tags[tag] == req.body.tags[tag_])
						to_delete = false
				}
				if (to_delete) {
					sql.set ('DELETE FROM `Tag` WHERE `User_id` = ? AND `Title` = ?', [req.session.user_id, req.session.tags[tag]], function () {})
					sql.set ('UPDATE `Tags` SET `Uses` = `Uses` - 1 WHERE `Title` = ?', [req.session.tags[tag]], function () {})
					req.session.tags[tag] = null
				}
			}
			var id = []
			for (var tag in req.body.tags) {
				if (/^\#[a-zA-Z0-9]{2,10}$/.test (req.body.tags[tag]) && req.session.tags.indexOf (req.body.tags[tag]) < 0) {
					id.push (tag)
					req.session.tags.push (req.body.tags[tag])
					sql.set ('INSERT INTO `Tag` (`User_id`, `Title`) VALUES (?, ?)', [req.session.user_id, req.body.tags[tag]], function () {})
					sql.get ('SELECT * FROM `Tags` WHERE `Title` = ?', [req.body.tags[tag]], function (rows) {
						var actual_tag = id.shift ()
						if (rows.length > 0)
							sql.set ('UPDATE `Tags` SET `Uses` = `Uses` + 1 WHERE `Tags_id` = ?', [rows[0].tags_id], function () {})
						else
							sql.set ('INSERT INTO `Tags` (`Title`) VALUES (?)', [req.body.tags[actual_tag]], function () {})
					})
				}
			}
			res.redirect ('/')
		} else {
			res.redirect ('/profil')
		}
	})
}
