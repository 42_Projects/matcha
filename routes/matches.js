'use strict'

var sql		= require ('../modules/sql')

module.exports = function (router, io)
{
	router.route (['/', '/index', '/index.html'])
	.get (function (req, res) {
		var infos = req.session.infos ? req.session.infos : ''
		var flag = req.session.flags ? req.session.flag : ''

		if (!req.session.user_id) {
			return res.render ('connection', {
				title: 'Matcha',
				infos: infos,
			})
		}

		sql.get ('SELECT `Title` \
					FROM `Tags` \
				   WHERE `Uses` > 0 \
				ORDER BY `Uses` DESC', function (tags) {
			return res.render ('matches', {
				title: 'Matcha',
				session: req.session,
				badges_history: [
					req.session.user_infos.visitPink_history_count,
					req.session.user_infos.likePink_history_count
				],
				badges: [
					req.session.user_infos.visitPink_history_count + req.session.user_infos.likePink_history_count,
					req.session.user_infos.messages_count
				],
				tags: tags
			})
		})
	})
}
